#include <iostream>

using std::cout;
using std::endl;

void zamijeniP(int *a, int *b) {
  int tmp = *a;
  *a = *b;
  *b = tmp;
}

void zamijeniR(int &a, int &b) {
  int tmp = a;
  a = b;
  b = tmp;
}

int main(void) {
  int broj1 = 2;
  int broj2 = 3;

  zamijeniP(&broj1, &broj2);
  cout << "broj1: " << broj1 << " ";
  cout << "broj2: " << broj2 << endl;
  zamijeniR(broj1, broj2);
  cout << "broj1: " << broj1 << " ";
  cout << "broj2: " << broj2 << endl;
}