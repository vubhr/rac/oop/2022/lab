#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;

struct Film {
  string naziv;
  int godinaIzdanja;
  int trajanje;
};

void prikaziFilm(const Film &film) {
  cout << film.naziv << " (" << film.godinaIzdanja << "), ";
  cout << film.trajanje / 60 << "h" << film.trajanje % 60;
  cout << endl;
}

void skratiFilm(Film &film) {
  film.trajanje = 60;
}

int main() {
  Film noviFilm = {"Joker", 2019, 122};
  prikaziFilm(noviFilm);
}