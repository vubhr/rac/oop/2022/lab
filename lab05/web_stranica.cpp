#include "web_stranica.hpp"

#include <iostream>

WebStranica::WebStranica(std::string url, int port, bool sigurno)
    : url(url), port(port), sigurno(sigurno) {
}

void WebStranica::prikazi() {
  if (sigurno) {
    std::cout << url << " (" << port << ") SIGURNO" << std::endl;
  } else {
    std::cout << url << " (" << port << ") NIJE SIGURNO" << std::endl;
  }
}
