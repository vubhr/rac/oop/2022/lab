#include <iostream>
#include <cmath>
#include "KompleksniBroj.hpp"

double KompleksniBroj::izracunajApsolutnuVrijednost() {
  return sqrt(pow(realni, 2) + pow(imaginarni, 2));
}

void KompleksniBroj::prikaziKompleksniBroj() {
  std::cout << "Re: " << realni << ", ";
  std::cout << "Im: " << imaginarni << std::endl;
}

void KompleksniBroj::zbrojiKompleksniBroj(KompleksniBroj broj) {
  this->realni = this->realni + broj.realni;
  this->imaginarni = this->imaginarni + broj.imaginarni;
}

double KompleksniBroj::getRealni() {
  return realni;
}

void KompleksniBroj::setRealni(double realni) {
  this->realni = realni;
}

double KompleksniBroj::getImaginarni() {
  return imaginarni;
}

void KompleksniBroj::setImaginarni(double imaginarni) {
  this->imaginarni = imaginarni;
}
