#include <iostream>
#include "KompleksniBroj.hpp"

int main() {
  KompleksniBroj broj1;
  broj1.setRealni(2.3);
  broj1.setImaginarni(4.1);

  broj1.prikaziKompleksniBroj();
  double apsolutno = broj1.izracunajApsolutnuVrijednost();
  std::cout << "Apsolutna vrijednost: " << apsolutno << std::endl;

  KompleksniBroj broj2;
  broj2.setRealni(1.5);
  broj2.setImaginarni(3.7);

  broj1.zbrojiKompleksniBroj(broj2);
  broj1.prikaziKompleksniBroj();
}