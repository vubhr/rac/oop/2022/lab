class KompleksniBroj {
public:
  double izracunajApsolutnuVrijednost();
  void prikaziKompleksniBroj();
  void zbrojiKompleksniBroj(KompleksniBroj broj);

  double getRealni();
  void setRealni(double realni);

  double getImaginarni();
  void setImaginarni(double imaginarni);

private:
  double realni;
  double imaginarni;
};
