#include <iostream>
#include "KompleksniBroj.hpp"

void prikaziKompleksniBroj(KompleksniBroj broj) {
  std::cout << "Re: " << broj.realni << ", ";
  std::cout << "Im: " << broj.imaginarni << std::endl;
}

KompleksniBroj zbrojiKompleksneBrojeve(KompleksniBroj broj1, KompleksniBroj broj2) {
  KompleksniBroj rezultat;
  rezultat.realni = broj1. realni + broj2.realni;
  rezultat.imaginarni = broj1.imaginarni + broj2.imaginarni;
  return rezultat;
}
