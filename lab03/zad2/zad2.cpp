#include "KompleksniBroj.hpp"

int main() {
  KompleksniBroj broj1 { 23.3, 67.2 };
  KompleksniBroj broj2 { 13.2, -2.3 };  
  prikaziKompleksniBroj(broj1);

  KompleksniBroj zbroj = zbrojiKompleksneBrojeve(broj1, broj2);
  prikaziKompleksniBroj(zbroj);
}