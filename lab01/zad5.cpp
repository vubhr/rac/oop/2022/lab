#include <iostream>

#define N 5

using std::cout;
using std::cin;
using std::endl;

void prikaziPolje(int polje[], int n) {
  for (int i = 0; i < n - 1; i++) {
    cout << polje[i] << ",";
  }
  cout << polje[n - 1] << endl;
}

int main() {
  int brojevi[N] = { 2, 3, 6, 5, 3 };
  
  prikaziPolje(brojevi, N);
}