#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main() {
  int broj;
  cout << "Unesite broj: ";
  cin >> broj;

  if (broj < 1 || broj > 10) {
    return 0;
  }

  for (int i = 0; i <= broj; i++) {
    // brojke
    for (int j = 0; j < i; j++) {
      cout << j + 1;
    }

    // zvjezdice
    for (int j = 0; j < broj - i; j++) {
      cout << "*";
    }
    cout << endl;
  }
}