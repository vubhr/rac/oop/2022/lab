#include <iostream>

#define N 5

using std::cout;
using std::cin;
using std::endl;

void prikaziPolje(int polje[], int n) {
  for (int i = 0; i < n - 1; i++) {
    cout << polje[i] << ",";
  }
  cout << polje[n - 1] << endl;
}

void prikaziPolje(double polje[], int n) {
  for (int i = 0; i < n - 1; i++) {
    cout << polje[i] << ",";
  }
  cout << polje[n - 1] << endl;
}

int main() {
  int cijeliBrojevi[N] = { 2, 3, 6, 5, 3 };
  double decimalniBrojevi[N] = { 5.2, 1.0, 6.5, -2.9, 9.9 };
  
  prikaziPolje(cijeliBrojevi, N);
  prikaziPolje(decimalniBrojevi, N);
}