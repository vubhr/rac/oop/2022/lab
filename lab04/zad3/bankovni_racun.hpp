#include "osoba.hpp"

class BankovniRacun {
public:
  BankovniRacun(std::string ime, std::string prezime);

  void uplati(double iznos);
  void isplati(double iznos);
  void prikaziStanje();

private:
  Osoba osoba;
  double stanje;
};