#include <iostream>
#include "bankovni_racun.hpp"

BankovniRacun::BankovniRacun(std::string ime, std::string prezime) : osoba(ime, prezime) {
  stanje = 0.0;
}

void BankovniRacun::uplati(double iznos) {
  stanje = stanje + iznos;
}

void BankovniRacun::isplati(double iznos) {
  stanje = stanje - iznos;
}

void BankovniRacun::prikaziStanje() {
  std::cout << osoba.getIme() << " " << osoba.getPrezime();
  std::cout << ": " << stanje << " KN" << std::endl;
}