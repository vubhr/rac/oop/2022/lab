#include <iostream>
#include "motocikl.hpp"

Motocikl::Motocikl() : Motocikl("", "", 0) { }

Motocikl::Motocikl(std::string proizvodac, std::string model) : 
  Motocikl(proizvodac, model, 0) { }

Motocikl::Motocikl(std::string proizvodac, std::string model, int zapremnina) :
  proizvodac(proizvodac), model(model), zapremnina(zapremnina) {
    // dodatan inicijalizacijski kod
    if (zapremnina < 0) {
      zapremnina = 0;
    }
}
void Motocikl::prikaz() {
  std::cout << proizvodac << " " << model << " (" << zapremnina << ")" << std::endl;
}